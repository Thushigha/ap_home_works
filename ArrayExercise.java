package array;

import java.util.Arrays;
import java.util.Random;

public class ArrayExercise {

	public static void main(String[] args) {

		Random rand = new Random();
		int[] numbers = new int[10];
		int total = 0;

		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = rand.nextInt(100);
		}

		System.out.println(Arrays.toString(numbers));

		for (int i = 0; i < numbers.length; i++) {
			total += numbers[i];
		}
		System.out.println("\nThe total of 10 numbers is " + total + "\n");

		int avg = total / 10;
		System.out.println("The avarage of 10 numbers is " + avg + "\n");

		boolean found = false;
		int searchingNumber = 3;
		for (int x : numbers) {
			if (x == searchingNumber) {
				found = true;
				break;
			}
		}
		System.out.println("Does array contains searchingNumber ? " + found + "\n");

		int value = 0;
		int k = 0;
		for (int i = 0; i < numbers.length; i++) {

			if (numbers[i] == value) {
				k = i;
			} else {
				System.out.println("index not found");
				break;

			}
			System.out.println(k);
		}
		
		

	}

}